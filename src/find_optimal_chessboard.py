import numpy as np

# ## first length of rigid board (biggest)
# L1 = 52.0
# ## second length of rigid board (smallest)
# L2 = 39.0
# ## tolerance on L1 value for markers size
# tol = 4.0
# ## maximal number of cases on chessboard on L2
# Nmax = 10

## first length of rigid board (biggest)
L1 = 28.0
## second length of rigid board (smallest)
L2 = 20.0
## tolerance on L1 value for markers size
tol = 9.0
## maximal number of cases on chessboard on L2
Nmax = 10

Lvals = []
Lindex = []
LmSize = []
for k in range(2,Nmax):
  Ltst = []
  for j in range(Nmax):
    if k%2 == 0:
      val = 2*j+1
    else:
      val = 2*j
    if val < Nmax and val > 0:
        Ltst.append(val)

  vClose = -1
  Linds = []

  diffClose = np.inf
  for j in range(len(Ltst)):
    value = (L2/k)*Ltst[j]-(L1)
    if abs(value) < diffClose:
        diffClose = abs(value)
        if (diffClose < 2*tol):
            vClose = value
            Linds = [k,Ltst[j]]


  if vClose !=-1:
    Lvals.append(vClose)
    Lindex.append(Linds)
    LmSize.append(L2/Linds[0])


print(Lvals)
print(Lindex) 
print(LmSize)