#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import rospy

import matplotlib.pyplot as plt

from mpl_toolkits.mplot3d import Axes3D


from msg_srv_action_gestion.srv import SetStringWR


from opencv_pinhole_management.pinhole_utils import pinholeUtils

from sensor_msgs.msg import CameraInfo


import matplotlib as mpl

class test2d3dcalib():

    def display2D3D2D(self,Z):
        pos_err = np.array([])
        for z in Z:
            X = list(np.linspace(0,self.pU.width, self.NbePix))
            Y = list(np.linspace(0,self.pU.height,self.NbePix))
            pos2D = []
            allDepth = []
            for x in X:
                pos2D += [ [x,y] for y in Y]
                allDepth += [z for y in Y]

            
            debug3D = self.pU.from2Dto3DW(pos2D, allDepth,False) 
            pos2DM = np.array(pos2D)
            debug2D_aft = self.pU.from3Dto2DNS(debug3D,False)
            debug2D_aft_L = self.pU.M2L(debug2D_aft,True)
            debug3D_aft = self.pU.from2Dto3DW(debug2D_aft_L, list(debug3D[:,2]),False) 

            # diffVal = np.linalg.norm(pos2DM - debug2D_aft, axis = 1)
            diffVal = np.linalg.norm(debug3D - debug3D_aft, axis = 1)

            M = np.c_[debug3D,diffVal]

            if pos_err.shape[0] == 0:
                pos_err = M
            else:
                pos_err = np.r_[pos_err,M]
        return(pos_err)

    def display3D2D3D(self,Z):
        pos_err = np.array([])        
        for z in Z:
            extremums_2D = [ [0,0], 
                            [self.pU.width, self.pU.height] ]
            z_ext = [z for y in extremums_2D]
            extremums_3D = self.pU.from2Dto3DW(extremums_2D,z_ext,True) 

            xMin = extremums_3D[0][0]
            yMin = extremums_3D[0][1]
            xMax = extremums_3D[1][0]
            yMax = extremums_3D[1][1]

            nbePts = int( min(xMax-xMin,yMax-yMin)/self.cartRes )

            X = list(np.linspace(xMin,xMax, nbePts))
            Y = list(np.linspace(yMin,yMax, nbePts))             

            pos3D = []
            for x in X:
                new3D = [ [x,y,z] for y in Y]     
                debug2D = self.pU.from3Dto2DNS(new3D,False) 
                pose3D_correct = []
                for k in range(len(debug2D)):
                    xOk =  (debug2D[k][0]>= 0 and debug2D[k][0] <= self.pU.width)
                    yOk =  (debug2D[k][1]>= 0 and debug2D[k][1] <= self.pU.height)
                    if (xOk and yOk):
                        pose3D_correct.append(new3D[k])
                pos3D += pose3D_correct

            debug2D = self.pU.from3Dto2DNS(pos3D)
            # print(debug2D)
            pos3DM = np.array(pos3D)
            debug3D_aft = self.pU.from2Dto3DW(debug2D, list(pos3DM[:,2]),False) 
            
            diffVal = np.linalg.norm(pos3DM - debug3D_aft, axis = 1)

            M = np.c_[pos3DM,diffVal]

            if pos_err.shape[0] == 0:
                pos_err = M
            else:
                pos_err = np.r_[pos_err,M]

        return(pos_err)



    def __init__(self):
        

        rospy.init_node("tst2d3d", anonymous=True)


        nodeName = rospy.get_name()
        nodeName+="/"

        self.nodeName = nodeName

        yamlPath =  rospy.get_param(nodeName + 'calib_file', "")

        # self.ciSub = rospy.Subscriber(cameraSub+"/camera_info",CameraInfo,self.ciInfos)
        self.pU = pinholeUtils()
        ci_msg = self.pU.get_cam_info_from_yaml(yamlPath)
        self.pU.set_cam_info_msg(ci_msg)

        self.setUp = self.pU.gotCI

        # rospy.loginfo("Setup : ")
        # print(self.setUp)

        self.done = False

        self.pos_err = np.array([])

        self.NbePix = 40
        self.cartRes = 0.02

        Zmin = 0.165
        #Zmax = Zmin + 0.1
        Zmax = 0.9      
        NZ = int((Zmax-Zmin)/self.cartRes)



        allTypeTest = ["2D3D2D","3D2D3D"]

        typeTest = " "# allTypeTest[1]

        L_all_print = []




        while not rospy.is_shutdown() and not self.done:

            if self.setUp:
                rospy.loginfo("Start computing...")
                Z = list(np.linspace(Zmin,Zmax,NZ))
                if (typeTest == "2D3D2D"):
                    pos_err = self.display2D3D2D(Z)
                    L_all_print.append(pos_err)

                elif typeTest == "3D2D3D":
                    pos_err = self.display3D2D3D(Z)

                    L_all_print.append(pos_err)
                
                else:
                    L_all_print.append(self.display2D3D2D(Z))
                    L_all_print.append(self.display3D2D3D(Z))
                self.done = True
        # print(self.pos_err.shape)

        print(L_all_print[1].shape)
        #exit()

        for k in range(len(L_all_print)):
            self.pos_err = L_all_print[k]

            fig = plt.figure(k)

            ax = fig.add_subplot(111, projection='3d')

            errMax =  np.max(self.pos_err[:,3])
            rospy.loginfo("Max error : %f", errMax)
            rospy.loginfo("Median error : %f", np.median(self.pos_err[:,3]))

            colors = mpl.cm.coolwarm(self.pos_err[:,3]/errMax)
            colmap = mpl.cm.ScalarMappable(cmap=mpl.cm.coolwarm)
            colmap.set_array(self.pos_err[:,3])

            ax.scatter(self.pos_err[:,0], self.pos_err[:,1], self.pos_err[:,2], c=colors,marker="o")

            cb = fig.colorbar(colmap)
            ax.set_xlabel('X Label')
            ax.set_ylabel('Y Label')
            ax.set_zlabel('Z Label')

        plt.show()





    def ciInfo(self,msg):

        self.pU.set_cam_info_msg(msg)

        self.setUp = True



if __name__ == "__main__":
    test2d3dcalib()