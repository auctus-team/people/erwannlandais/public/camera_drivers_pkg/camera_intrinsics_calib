#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## check : https://github.com/ros-perception/image_pipeline/blob/4a4760d2d970da42fb2647ae666e989cde5683f2/camera_calibration/scripts/tarfile_calibration.py#L42
## https://github.com/ros-perception/image_pipeline/blob/noetic/camera_calibration/src/camera_calibration/camera_checker.py
## https://github.com/ros-perception/image_pipeline/blob/noetic/camera_calibration/src/camera_calibration/calibrator.py

import sys
import cv2
import numpy as np
import rospy

import subprocess

from msg_srv_action_gestion.srv import SetStringWR

import yaml


import os
import numpy

import cv2
import cv_bridge
import tarfile

from camera_calibration.calibrator import MonoCalibrator, StereoCalibrator, CalibrationException, ChessboardInfo, Patterns, MonoDrawable

import sensor_msgs.srv

from msg_srv_action_gestion.srv import SetStringWR


import os
from glob import glob


class calibIntrinsicsManager():

    def __init__(self):
        

        rospy.init_node("cim", anonymous=True)


        nodeName = rospy.get_name()
        nodeName+="/"

        self.nodeName = nodeName

        self.images = []
        self.calibrator = None

        self.rvecs = []
        self.tvecs = []

        self.quit = False

        from optparse import OptionParser
        parser = OptionParser()

        parser.add_option("--mono", default=True, action="store_true", dest="mono",
                        help="Monocular calibration only. Calibrates left images.")
        parser.add_option("-s", "--size", default=[], action="append", dest="size",
                        help="specify chessboard size as NxM [default: 8x6]")                    
        parser.add_option("-q", "--square", default=[], action="append", dest="square",
                        help="specify chessboard square size in meters [default: 0.108]")
        parser.add_option("--upload", default=False, action="store_true", dest="upload",
                        help="Upload results to camera(s).")
        parser.add_option("--fix-principal-point", action="store_true", default=False,
                        help="fix the principal point at the image center")
        parser.add_option("--fix-aspect-ratio", action="store_true", default=False,
                        help="enforce focal lengths (fx, fy) are equal")
        parser.add_option("--zero-tangent-dist", action="store_true", default=False,
                        help="set tangential distortion coefficients (p1, p2) to zero")
        parser.add_option("-k", "--k-coefficients", type="int", default=2, metavar="NUM_COEFFS",
                        help="number of radial distortion coefficients to use (up to 6, default %default)")
        parser.add_option("--visualize", action="store_true", default=False,
                        help="visualize rectified images after calibration")
        parser.add_option("-a", "--alpha", type="float", default=1.0, metavar="ALPHA",
                        help="zoom for visualization of rectifies images. Ranges from 0 (zoomed in, all pixels in calibrated image are valid) to 1 (zoomed out, all pixels in  original image are in calibrated image). default %default)")
        parser.add_option("-m", "--max_chessboard-speed", type="float", default=-1, help="max_chessboard_speed" )
        parser.add_option("--sy", "--save_yaml", default=False, action="store_true", dest="save_yaml",
                        help="Save camera info into yaml file")
        parser.add_option("--uc", "--use_charuco", default=False, action="store_true", dest="use_charuco",
                        help="Use charuco board")


        self.options, args = parser.parse_args()        

        self.pathfile = rospy.get_param(nodeName + "path", "")
        self.cipath = ""

        size = rospy.get_param(nodeName + "chess_size", "")
        sqr = rospy.get_param(nodeName + "chess_square","")
        self.aruco_dict = rospy.get_param(nodeName + "aruco_dict","")
        self.aruco_sqr =  rospy.get_param(nodeName + "aruco_square",-1)


        #print(self.aruco_sqr)

        use_charuco = rospy.get_param(nodeName + "use_charuco",False)
        if use_charuco:
            self.pattern = "charuco"
            self.pattern_class = Patterns.ChArUco
            self.options.use_charuco = True

        else:
            self.pattern = "chessboard"
            self.pattern_class = Patterns.Chessboard
            self.options.use_charuco = False
        if size != "":
            self.options.size.append(size)
        if sqr != "":
            self.options.square.append(sqr)


        self.filename_list = []

        self.LlinRMS = []
        self.LrepRMS = []

        self.D_name_err = {}


        self.inCall = False
        self.result = ""

        self.get_request = rospy.Service("/calib_intrinsics",SetStringWR, self.get_calib_call)

        rospy.spin()

    def display(self,win_name, img):
        # cv2.namedWindow(win_name, cv2.WND_PROP_FULLSCREEN)
        # cv2.setWindowProperty(win_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        cv2.namedWindow(win_name, cv2.WINDOW_NORMAL)
        cv2.imshow( win_name,  numpy.asarray( img[:,:] ))
        k = cv2.waitKey(0)
        cv2.destroyWindow(win_name)
        if k in [27, ord('q')]:
            self.quit = True



    def better_image_corners(self, gray):
        #(ok, corners, ids, b) = self.calibrator.get_corners(gray)
        scrib_mono, corners, downsampled_corners, ids, board, (x_scale, y_scale) = self.calibrator.downsample_and_detect(gray)
        ok = (len(corners) != 0)
        if ok:
            return corners, ids
        else:
            return None, None


    def image_corners(self, gray):
        (ok, corners, ids, b) = self.calibrator.get_corners(gray)
        if ok:
            return corners, ids
        else:
            return self.better_image_corners(gray)


    def matchImagePoints(self,charuco_board, charuco_corners, charuco_ids):
        objPoints = []
        imgPoints = []
        for i in range(0, len(charuco_ids)):
            index = charuco_ids[i]
            objPoints.append(charuco_board.getChessboardCorners()[index])
            # objPoints[-1][0][1] = charuco_board.getRightBottomCorner()[1] - objPoints[-1][0][1]  #  set old axis direction
            imgPoints.append(charuco_corners[i])
        return np.array(objPoints), np.array(imgPoints)


    def handle_monocular(self,name, msg, board, i = np.inf):

        (image, camera) = msg
        gray = image #self.calibrator.mkgray(image)
        C, ids = self.image_corners(gray)

        #print(len(C))

        if C is not None:
            ok_use = False
            linearity_rms = self.calibrator.linear_error(C, ids, board)

            dist_coeffs = np.array(camera.D)            
            camera_matrix = numpy.array( [ [ camera.P[0], camera.P[1], camera.P[2]  ],
                                        [ camera.P[4], camera.P[5], camera.P[6]  ],
                                        [ camera.P[8], camera.P[9], camera.P[10] ] ] )

            # Add in reprojection check
            if (self.pattern == "charuco"):
                ## inspired from https://github.com/opencv/opencv_contrib/issues/2921
                ## and http://amroamroamro.github.io/mexopencv/opencv_contrib/aruco_calibrate_camera_charuco_demo.html
                dC= dist_coeffs
                cM = camera_matrix

                newCM = cM
                undistorted = cv2.undistort(image,cM,dC,newCM)
                # print("cM : ", cM)
                # print("newCM : ", newCM)                
                dC = np.zeros_like(dC)
                cM = newCM

                # Get and refine markerCorners (= corners of tiles)
                markerCorners,markersIds,rejected = cv2.aruco.detectMarkers(undistorted,board.aruco_dict)
                #markerCorners,markersIds,_,_ = cv2.aruco.refineDetectedMarkers(undistorted,board.charuco_board,markerCorners,markersIds,rejected,cM,dC)
                ##

                try : 
                    if ( markersIds.shape[0] > 0):
                        retval, charucoCorners, charucoIds = cv2.aruco.interpolateCornersCharuco(markerCorners,markersIds,undistorted,board.charuco_board,cM,dC)
                        # print("====")
                        # print(retval)
                        if (charucoIds.shape[0] > 0):

                            # object_points, image_points = self.matchImagePoints(board.charuco_board,
                            #                                                     charucoCorners,
                            #                                                     charucoIds)

                            charucoIdsPic = self.reshape_charuco_ids(charucoIds,board.charuco_board)

                            ## FONCTION FAUSSE !!!! :<
                            ## A CORRIGER SOIT MÊME!!!
                            object_points, image_points = cv2.aruco.getBoardObjectAndImagePoints(
                                board.charuco_board,
                                charucoCorners,
                                charucoIdsPic
                            )


                            retval,rot,trans = cv2.aruco.estimatePoseCharucoBoard(charucoCorners, charucoIds, board.charuco_board, cM, dC, np.array([]), np.array([]), False)                        

                            if (retval):
                                ## solve PNP_IPPE_SQUARE marche pas :(
                                ok_use, rot, trans = cv2.solvePnP(object_points, image_points, camera_matrix, dist_coeffs, rot, trans, True )                                            

                                # print(object_points[0])
                                # print(image_points[0])


                                dist_coeffs = dC
                                camera_matrix = cM
                                # if len(self.rvecs) > i:
                                #     rot = self.rvecs[i]
                                #     trans = self.tvecs[i]

                    else:

                        image_points = C
                        object_points = self.calibrator.mk_object_points([board], use_board_size=True)[0]
                        ok, rot, trans = cv2.solvePnP(object_points, image_points, camera_matrix, dist_coeffs)                
                        ok_use = True
                except Exception:
                    ok_use = False

            # dist_coeffs = numpy.zeros((4, 1))

            if (ok_use):

                # Convert rotation into a 3x3 Rotation Matrix
                rot3x3, _ = cv2.Rodrigues(rot)
                # Reproject model points into image
                object_points_world = numpy.asmatrix(rot3x3) * numpy.asmatrix(object_points.squeeze().T) + numpy.asmatrix(trans)
                reprojected_h = camera_matrix * object_points_world
                reprojected   = (reprojected_h[0:2, :] / reprojected_h[2, :])
                reprojection_errors = image_points.squeeze().T - reprojected
                reprojection_rms = numpy.sqrt(numpy.sum(numpy.array(reprojection_errors) ** 2) / numpy.product(reprojection_errors.shape))


                ## 2e poss (from https://docs.opencv.org/4.x/dc/dbb/tutorial_py_calibration.html)
                imgpoints2, _ = cv2.projectPoints(object_points, rot3x3, trans, camera_matrix, dist_coeffs)
                reprojection_rms2 = cv2.norm(image_points, imgpoints2, cv2.NORM_L2)/len(imgpoints2)

                # if len(self.reproj_err) > i:
                #     reprojection_rms2 = self.reproj_err[i]

                # Print the results
                if linearity_rms is not None and reprojection_rms is not None and reprojection_rms2 is not None:
                    print("Image : %s : Linearity RMS Error: %.3f Pixels      Reprojection RMS Error: %.3f Pixels or %.3f Pixels" % (name, linearity_rms, reprojection_rms, reprojection_rms2))

                    self.LlinRMS.append(linearity_rms)
                    self.LrepRMS.append(reprojection_rms)

                    self.D_name_err[name] = [linearity_rms,reprojection_rms]
        else:
            print('Image : %s : no chessboard'% (name))


    def reshape_charuco_ids(self,charucoIds,charucoBoard):
        """!
        
        0 ==> maxRow
        
        
        """
        size = list(charucoBoard.getChessboardSize())
        ## size = (5,7)
        nPerRow = size[0]-1
        nPerCol = size[1] - 1

        maxId = (size[0]-1)*(size[1]-1)-1
        idTreated = 0

        iC = 0
        indexRow = size[1]-2

        nAdd = indexRow*nPerRow

        newCharucoIds = np.zeros(charucoIds.shape, charucoIds.dtype)

        while not rospy.is_shutdown() and idTreated <= maxId:
            if iC > nPerRow-1:
                indexRow-= 2
                nAdd = indexRow*nPerRow
                iC = 0

            pos = np.where(charucoIds == idTreated)
            if pos[0].shape[0] != 0:

                id_index = pos[0][0]
                #print("bef : ",charucoIds[id_index,0])                
                newCharucoIds[id_index,0] = int(charucoIds[id_index,0] + nAdd)
                #print("aft : ",charucoIds[id_index,0])    
            
            iC+=1
            idTreated+=1
        
        #print(newCharucoIds)
        return(newCharucoIds)
    
    def correct_object_points(self,charucoIds,charucoBoard,object):
        newObject = np.zeros(object.shape)
        newCharucoId = self.reshape_charuco_ids(charucoIds,charucoBoard)

        print(object.shape)
        print(charucoIds.shape)

        ## normally
        iC = 0
        for i in range(charucoIds.shape[0]):
            concernedId = charucoIds[i,0]
            pos = np.where(newCharucoId == concernedId)
            if pos[0].shape[0] != 0:
                #print(pos)

                #print( range(2*pos[0][0],2*(pos[0][0]+2) ) )
                newObject[4*pos[0][0]:4*(pos[0][0]+1), :] = object[4*iC:4*(iC+1),:]
                iC+=1
        return(newObject)



    def calibrate_charuco_board(self, images, board):
        """!
        
        Starting from chosen_images, calibrate by : 
            * First, get an approximate intrinsics by Aruco Markers only
            * Use this approximate to get Charuco boards
            * Use the selected Charuco boards to get the intrinsics
        
        Inspired from http://amroamroamro.github.io/mexopencv/opencv_contrib/aruco_calibrate_camera_charuco_demo.html
        """

        # Get and refine markerCorners (= corners of tiles)
        # image = self.calibrator.mkgray(image)

        saved = False
        allArucoCorners = []
        allArucoIds = []
        allImgs = []
        allMarkersOnImage = []


        size = (images[0].shape[1], images[0].shape[0])

        recalledShape = None

        for image in images:
            markerCorners,markersIds,rejected = cv2.aruco.detectMarkers(image,board.aruco_dict)
            ## no refine, bug bc matchImagePoints
            
            #markerCorners,markersIds,_,_ = cv2.aruco.refineDetectedMarkers(image,board.charuco_board,markerCorners,markersIds,rejected)        

            #print(markerCorners)
            try:
                if ( markersIds.shape[0] > 0):
                    retval, charucoCorners, charucoIds = cv2.aruco.interpolateCornersCharuco(markerCorners,markersIds,image,board.charuco_board)
                    if (charucoIds.shape[0] > 0):       
                        markerCorners = np.array(list(markerCorners))
                        markersIds = np.array(list(markersIds))
                        if not saved : 
                            allArucoCorners = markerCorners
                            allArucoIds = markersIds
                            saved = True
                            recalledShape = list(markerCorners.shape)
                        else:
                            recalledShape[0] = list(markerCorners.shape)[0]
                            if markerCorners.shape != recalledShape:
                                # print("call reshpae!")
                                # print(recalledShape)
                                # print(markerCorners.shape)
                                markerCorners = markerCorners.reshape( tuple(recalledShape)  )

                            # print(allArucoCorners.shape)
                            # print(allArucoIds.shape)
                            # print(markersIds.shape)
                            # print(markerCorners.shape)
                            allArucoCorners = np.vstack( (allArucoCorners,markerCorners))
                            allArucoIds = np.vstack( (allArucoIds,markersIds))

                        allImgs.append(image)
                        allMarkersOnImage.append( len(markersIds) )

            except Exception:
                ok_use = False
        allCharucoCorners = []
        allCharucoIds = []
        cM = numpy.eye(3, dtype=numpy.float64)
        dC = None
        saved = False
        cnt = 0

        allMarkersOnImage = np.array(allMarkersOnImage)

        if len(allImgs) > 0:
            retVal,cM,dC,rvecs,tvecs = cv2.aruco.calibrateCameraAruco(allArucoCorners,allArucoIds,allMarkersOnImage,board.charuco_board,size,cM,dC, flags = self.calibrator.calib_flags)
            rospy.loginfo("Intermediate calib on %d pictures", len(allImgs))            
            self.result += "intermediate calib on %d pictures ; "%len(allImgs)
            for i in range(len(allImgs)):
                image = allImgs[i]
                markerCorners,markersIds,rejected = cv2.aruco.detectMarkers(image,board.aruco_dict)
                #markerCorners,markersIds,_,_ = cv2.aruco.refineDetectedMarkers(image,board.charuco_board,markerCorners,markersIds,rejected,cM,dC)   

                ## other poss
                # markerCorners = allArucoCorners[i]
                # markersIds = allArucoIds[i]
                try:
                    if ( markersIds.shape[0] > 0):
                        retval, charucoCorners, charucoIds = cv2.aruco.interpolateCornersCharuco(markerCorners,markersIds,image,board.charuco_board,cM,dC)                
                        if (charucoIds.shape[0] > 0):   

                            allCharucoCorners.append(charucoCorners)
                            allCharucoIds.append(charucoIds)

                            # if not saved : 
                            #     allCharucoCorners = charucoCorners
                            #     allCharucoIds = charucoIds
                            #     saved = True
                            # else:
                            #     allCharucoCorners = np.vstack( (allCharucoCorners,charucoCorners))
                            #     allCharucoIds = np.vstack( (allCharucoIds,charucoIds))
                            cnt+=1
                except Exception:
                    a = 1
        if len(images) <= 4:
            cnt = 5
        if cnt>4:

            # allCharucoCorners = allCharucoCorners[::-1]
            # allCharucoIds = allCharucoIds[::-1]
            # print(allCharucoIds)
            # print(len(allCharucoCorners) )
            # print(allCharucoCorners[0].shape)
            reproj_err, cM_new, dC_new, rvecs, tvecs = cv2.aruco.calibrateCameraCharuco(
                    allCharucoCorners, allCharucoIds, board.charuco_board, size, cM, dC, rvecs, tvecs, flags = self.calibrator.calib_flags)            
            self.calibrator.distortion = dC_new
            self.calibrator.intrinsics = cM_new
            self.calibrator.R = numpy.eye(3, dtype=numpy.float64)
            self.calibrator.P = numpy.zeros((3, 4), dtype=numpy.float64)
            self.calibrator.size = size
            self.rvecs = rvecs
            self.tvecs = tvecs

            cnt = len(allCharucoIds)
            rospy.loginfo("Final calib on %d pictures (with reproj error of %f)", cnt, reproj_err)            
            self.result += "final calib on %d pictures (with reproj error of %f); "%(cnt,reproj_err)
            self.reproj_err = reproj_err

            self.calibrator.set_alpha(0.0)            
            self.calibrator.calibrated = True


                # print("====")
                # print(retval)

    def handle_stereo(self, msg, board):

        (lmsg, lcmsg, rmsg, rcmsg) = msg
        lgray = self.calibrator.mkgray(lmsg)
        rgray = self.calibrator.mkgray(rmsg)

        L, _ = self.image_corners(lgray)
        R, _ = self.image_corners(rgray)
        if L is not None and R is not None:
            epipolar = self.calibrator.epipolar_error(L, R)

            dimension = self.calibrator.chessboard_size(L, R, board, msg=(lcmsg, rcmsg))

            print("epipolar error: %f pixels   dimension: %f m" % (epipolar, dimension))
        else:
            print("no chessboard")

    def draw_charuco_board(self,msg):
        gray = self.calibrator.mkgray(msg)
        linear_error = -1

        # Get display-image-to-be (scrib) and detection of the calibration target
        scrib_mono, corners, downsampled_corners, ids, board, (x_scale, y_scale) = self.calibrator.downsample_and_detect(gray)

        if self.calibrator.calibrated:
            cM = self.calibrator.intrinsics
            dC = self.calibrator.distortion

            # Show rectified image
            # TODO Pull out downsampling code into function
            gray_remap = self.calibrator.remap(gray)
            gray_rect = gray_remap
            if x_scale != 1.0 or y_scale != 1.0:
                gray_rect = cv2.resize(gray_remap, (scrib_mono.shape[1], scrib_mono.shape[0]))

            scrib = cv2.cvtColor(gray_rect, cv2.COLOR_GRAY2BGR)

            if corners is not None:
                # Report linear error
                newCM = cM
                undistorted = cv2.undistort(scrib,cM,dC, newCM)
                undistorted_copy = undistorted.copy()

                dC = np.zeros_like(dC)
                cM = newCM
                # Draw rectified corners

                markerCorners,markersIds,_ = cv2.aruco.detectMarkers(undistorted,board.aruco_dict)
                # print(markerCorners[0])
                # print(markersIds[0])
                # print(board.charuco_board.getChessboardSize())

                try:
                    if ( markersIds.shape[0] > 0):
                        cv2.aruco.drawDetectedMarkers(undistorted_copy,markerCorners,markersIds,  np.array([0.0,0.0,255]))
                        ## retval : int value

                        #markersIds = markersIds[::-1]
                        retval, charucoCorners, charucoIds = cv2.aruco.interpolateCornersCharuco(markerCorners,markersIds,undistorted,board.charuco_board,cM,dC)

                        #charucoIds = self.reshape_charuco_ids(charucoIds,board.charuco_board)

                        # print(charucoCorners.shape)

                        # print(charucoCorners[0])
                        # print(charucoIds[0])
                        
                        # #charucoCorners = charucoCorners[::-1]
                        # #charucoIds = charucoIds[::-1]

                        # print(charucoCorners[0])
                        # print(charucoIds[0])
                        # print("====")
                        # print(retval)
                        charucoIdsPic = self.reshape_charuco_ids(charucoIds,board.charuco_board)
                        #charucoIdsPic = charucoIds

                        # print(charucoIdsPic)

                        object_points, image_points = cv2.aruco.getBoardObjectAndImagePoints(
                            board.charuco_board,
                            charucoCorners,
                            charucoIdsPic
                        )
                        #print(charucoIds)
                        # print(image_points[0:2])
                        # print(object_points[0:2])
                        # print(image_points.shape)
                        # print(object_points.shape)

                        # new_object_points = self.correct_object_points(charucoIds,board.charuco_board,object_points)

                        # print(object_points[0])
                        # print(new_object_points[0])
                        # print(image_points[0])

                        if (charucoIds.shape[0] > 0):
                            cv2.aruco.drawDetectedCornersCharuco(undistorted_copy, charucoCorners, charucoIdsPic,np.array([255,0.0,0.0]))
                            retval,rvec,tvec = cv2.aruco.estimatePoseCharucoBoard(charucoCorners, charucoIdsPic, board.charuco_board, cM, dC, np.array([]), np.array([]), False)
                            ## retval : true if ok

                            if (retval):
                                cv2.drawFrameAxes(undistorted_copy,cM,dC,rvec,tvec,0.1)
                except Exception:
                    a = 1        
        rv = MonoDrawable()
        rv.scrib = undistorted_copy

        return(rv)





    def cal_and_check(self,boards, mono = False, upload = False, calib_flags = 0, visualize = False, alpha=1.0, yaml_save=False):
        
        self.result = ""
        if mono:
            self.calibrator = MonoCalibrator(boards, calib_flags,0, self.pattern_class)
        else:
            self.calibrator = StereoCalibrator(boards, calib_flags)

        self.calibrator.max_chessboard_speed = self.options.max_chessboard_speed
        self.chosen_names = []
        self.chosen_images = []

        for k in range(len(self.images)):
            Limg = len(self.calibrator.good_corners)

            bridge = cv_bridge.CvBridge()
            try:
                msg=bridge.cv2_to_imgmsg(self.images[k], "mono8")
            except cv_bridge.CvBridgeError as e:
                print(e)
            self.calibrator.handle_msg(msg)

            if (Limg !=  len(self.calibrator.good_corners)):
                rospy.loginfo("%s added.",self.all_names[k])
                self.chosen_names.append(self.all_names[k])
                self.chosen_images.append(self.images[k])
            else:
                rospy.loginfo("%s failed.",self.all_names[k])
        
        self.images = self.chosen_images
        self.all_names = self.chosen_names
                

        if (len(self.images) > 0):
            rospy.loginfo("Start calibration process on %d pictures", len(self.images))
            self.result += "Calib on %d pictures ; "%len(self.images)

            if (self.pattern == "charuco"):
                self.calibrate_charuco_board(self.images,boards[0])
            if not self.calibrator.calibrated:
                self.calibrator.cal(self.images)
                rospy.loginfo("Calibration done")                
            else:
                rospy.loginfo("Calibration done by Aruco => Charuco")                     

            info = self.calibrator.as_message()

            print("CI : ", info)

            ## display errors

            self.LlinRMS = []
            self.LrepRMS = []
            self.D_name_err = {}
            for i in range(len(boards)):
                for j in range(len(self.images)):
                    if mono:
                        msg = (self.images[j], info)
                        print("Check error of : ", self.all_names[j])

                        self.handle_monocular( self.all_names[j] , msg, boards[i], j )
                    else:
                        ## TODO
                        a= 1

            medlinRMS = np.inf
            replinRMS = np.inf


            if len(self.D_name_err) > 0:  
                medlinRMS = np.median(self.LlinRMS)
                replinRMS =  np.median(self.LrepRMS)

                self.result += "Median lin RMS : %f ; "% medlinRMS
                self.result += "Median reproj RMS : %f"% replinRMS


            



            if visualize:
                rospy.loginfo("====== WORST PICTURES ==========")

                #Show worst rectified images
                self.calibrator.set_alpha(alpha)

                if mono:
                    i = 0
                    cnt = 0
                    self.quit = False
                    while not rospy.is_shutdown() and i < len(self.images) and not self.quit:
                            if self.all_names[i] in self.D_name_err:
                                name = self.all_names[i]
                                linearity_rms, reprojection_rms = self.D_name_err[name]
                                if linearity_rms > medlinRMS and reprojection_rms > replinRMS:
                                    bridge = cv_bridge.CvBridge()
                                    try:
                                        msg=bridge.cv2_to_imgmsg(self.images[i], "mono8")
                                    except cv_bridge.CvBridgeError as e:
                                        print(e)
                                    
                                    print("Image : %s : Linearity RMS Error: %.3f Pixels      Reprojection RMS Error: %.3f Pixels" % (name, linearity_rms, reprojection_rms))

                                    #handle msg returns the recitifed image with corner detection once camera is calibrated.

                                    if self.pattern == "charuco":
                                        drawable = self.draw_charuco_board(msg)

                                    else:
                                        drawable=self.calibrator.handle_msg(msg)
                                    vis=numpy.asarray( drawable.scrib[:,:])
                                    #Display. Name of window:f
                                    self.display(self.all_names[i], vis)
                                    cnt+=1

                            i+= 1
                    if (cnt == 0):
                        i = 0
                        self.quit = False
                        while not rospy.is_shutdown() and i < min(len(self.images),10) and not self.quit:
                            if self.all_names[i] in self.D_name_err:
                                name = self.all_names[i]                        
                                bridge = cv_bridge.CvBridge()
                                try:
                                    msg=bridge.cv2_to_imgmsg(self.images[i], "mono8")
                                except cv_bridge.CvBridgeError as e:
                                    print(e)
                                
                                #print("Image : %s : Linearity RMS Error: %.3f Pixels      Reprojection RMS Error: %.3f Pixels" % (name, linearity_rms, reprojection_rms))

                                #handle msg returns the recitifed image with corner detection once camera is calibrated.

                                if self.pattern == "charuco":
                                    drawable = self.draw_charuco_board(msg)

                                else:
                                    drawable=self.calibrator.handle_msg(msg)
                                vis=numpy.asarray( drawable.scrib[:,:])
                                #Display. Name of window:f
                                self.display(self.all_names[i], vis)
                                i+= 1
                else:
                    ## TODO
                    a = 2
            if upload: 
                # self.result+="\n"
                # self.result+= str(info)
                if mono:
                    set_camera_info_service = rospy.ServiceProxy("%s/set_camera_info" % rospy.remap_name("camera"), sensor_msgs.srv.SetCameraInfo)

                    response = set_camera_info_service(info)
                    if not response.success:
                        raise RuntimeError("connected to set_camera_info service, but failed setting camera_info")
                else:
                    set_left_camera_info_service = rospy.ServiceProxy("%s/set_camera_info" % rospy.remap_name("left_camera"), sensor_msgs.srv.SetCameraInfo)
                    set_right_camera_info_service = rospy.ServiceProxy("%s/set_camera_info" % rospy.remap_name("right_camera"), sensor_msgs.srv.SetCameraInfo)

                    response1 = set_left_camera_info_service(info[0])
                    response2 = set_right_camera_info_service(info[1])
                    if not (response1.success and response2.success):
                        raise RuntimeError("connected to set_camera_info service, but failed setting camera_info")
            
            if yaml_save:
                ost_format = self.calibrator.ost()
                with open(
                    self.cipath,
                    "w",
                ) as file:
                    file.write(ost_format)
                Lsp = self.cipath.split("/")

                name = Lsp[-1].split(".")[0]
                path = ""
                for k in range(len(Lsp)-1):
                    path+="/" + Lsp[k]
                path = path[1:] + "/"   
                yml_path = path  + name + ".yaml"
                os.system("rosrun  camera_calibration_parsers convert  %s %s"%(self.cipath, yml_path) )
                os.system("rm -f %s"%self.cipath)
                # yml = self.calibrator.yaml()

                # with open(
                #     self.cipath,
                #     "w",
                # ) as file:
                #     yaml.dump(yml, file, default_flow_style=False)
        else:
            rospy.loginfo("No chessboard found on all the given pictures. Quit.")

    def get_calib_call(self,msg):
        
        if not self.inCall:
            self.inCall = True

            data = msg.data.split("|")
            sqcall = False
            
            for k in range(len(data)):

                keyVal = data[k].split("#")

                if keyVal[0] == "m":
                    self.options.mono = bool( int(keyVal[1]) )
                

                if keyVal[0] == "sq":
                    if not sqcall:
                        self.options.square = []
                        self.options.size = []
                        sqcall= True
                    self.options.square.append(keyVal[1])

                if keyVal[0] == "sz":
                    if not sqcall:
                        self.options.square = []
                        self.options.size = []
                        sqcall= True                   
                    self.options.size.append(keyVal[1])

                if keyVal[0] == "up":
                    self.options.upload = bool( int(keyVal[1]) )

                if keyVal[0] == "fpp":
                    self.options.fix_principal_point = bool( int(keyVal[1]) )
                
                if keyVal[0] == "far":
                    self.options.fix_aspect_ratio =  bool( int(keyVal[1]) )
                
                
                if keyVal[0] == "ztd":
                    self.options.zero_tangent_dist =  bool( int(keyVal[1]) )
                

                if keyVal[0] == "kc":
                    self.options.k_coefficients = float(keyVal[1])
                
                if keyVal[0] == "v":
                    self.options.visualize =  bool( int(keyVal[1]) )
                

                if keyVal[0] == "a":
                    self.options.alpha = float(keyVal[1])

                if keyVal[0] == "msc":
                    self.options.max_chessboard_speed = float(keyVal[1])
                if keyVal[0] == "sy":
                    self.options.save_yaml = bool( int(keyVal[1]) )


                if keyVal[0] == "path":
                    self.pathfile = keyVal[1]

                if keyVal[0] == "uc":
                    self.options.use_charuco =  bool( int(keyVal[1]) )                  
            
            rospy.loginfo("Parsing done.")
            print(self.options)
            self.manage_calib()

            self.inCall = False
        return(True,self.result)



    def manage_calib(self):

        self.rvecs = []
        self.tvecs = []
        self.reproj_err = []
        
        
        if len(self.options.size) != len(self.options.square):
            rospy.logwarn("Number of size and square inputs must be the same!")
        
        if self.options.use_charuco:
            self.pattern = "charuco"
            self.pattern_class = Patterns.ChArUco
        else:
            self.pattern = "chessboard"
            self.pattern_class = Patterns.Chessboard

        # if not self.options.square:
        #     self.options.square.append("0.108")
        #     self.options.size.append("8x6")

        boards = []
        for (sz, sq) in zip(self.options.size, self.options.square):

            dim = float(sq)
            size = tuple([int(c) for c in sz.split('x')])
            n_cols = size[0]
            n_rows = size[1]
            marker_size = 0.0
            aruco_dict = None

            if self.pattern == "charuco":
                marker_size = (self.aruco_sqr)

                #print("marker size : ", marker_size)
                
                aruco_dict = self.aruco_dict


            info = ChessboardInfo(self.pattern, n_cols, n_rows,dim,marker_size,aruco_dict)

            boards.append(info)

        if not boards:
            rospy.logwarn("Must supply at least one chessboard")

        self.all_names = []
        self.accepted_ext = ["*.png","*.jpg"]
        self.all_filenames = []
        for ext in self.accepted_ext:

            self.filename_list = glob(os.path.join(self.pathfile, ext))
            self.filename_list.sort()

            self.all_names += [self.filename_list[k].split("/")[-1] for k in range(len(self.filename_list))  ]
            self.all_filenames+= self.filename_list
        print(self.all_names)

        self.images = []
        #print(self.all_names)

        # for filename in filename_list:
        #     file_bytes = numpy.asarray(bytearray(filename), dtype=numpy.uint8)
        #     im=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)            


        self.images = [cv2.imread(filename, cv2.IMREAD_GRAYSCALE) for filename in self.all_filenames ]

        num_ks = self.options.k_coefficients

        calib_flags = 0
        if self.options.fix_principal_point:
            calib_flags |= cv2.CALIB_FIX_PRINCIPAL_POINT
        if self.options.fix_aspect_ratio:
            calib_flags |= cv2.CALIB_FIX_ASPECT_RATIO
        if self.options.zero_tangent_dist:
            calib_flags |= cv2.CALIB_ZERO_TANGENT_DIST
        if (num_ks > 3):
            calib_flags |= cv2.CALIB_RATIONAL_MODEL
        if (num_ks < 6):
            calib_flags |= cv2.CALIB_FIX_K6
        if (num_ks < 5):
            calib_flags |= cv2.CALIB_FIX_K5
        if (num_ks < 4):
            calib_flags |= cv2.CALIB_FIX_K4
        if (num_ks < 3):
            calib_flags |= cv2.CALIB_FIX_K3
        if (num_ks < 2):
            calib_flags |= cv2.CALIB_FIX_K2
        if (num_ks < 1):
            calib_flags |= cv2.CALIB_FIX_K1

        if self.options.save_yaml:
            self.cipath = self.pathfile + "/../ost_opti.ini"

        self.cal_and_check(boards, self.options.mono, self.options.upload, calib_flags, self.options.visualize, self.options.alpha, self.options.save_yaml)

if __name__ == "__main__":
    calibIntrinsicsManager()