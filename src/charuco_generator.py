#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## from https://github.com/opencv/opencv_contrib/issues/3291


## CCL : 
## Marker : ID0 en haut à gauche, ID max en bas à droite
## https://github.com/opencv/opencv_contrib/issues/3175 ; 
## IDs des coins et IDs des markers doivent normalement matcher.
import cv2

if __name__ == "__main__":

    aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)
    charuco_board = cv2.aruco.CharucoBoard_create(
        squaresX = 5,
        squaresY = 7,
        squareLength = 4,
        markerLength = 3,
        dictionary = aruco_dict
    )
    img_board = charuco_board.draw((50*5,50*7), marginSize=1, borderBits=1)
    cv2.imwrite('charuco_OpenCV-' + cv2.__version__ + '.png', img_board)

