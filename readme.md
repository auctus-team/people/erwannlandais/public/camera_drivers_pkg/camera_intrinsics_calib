# Description

This package is based on the process of the following package : https://github.com/ros-perception/image_pipeline/tree/rolling/camera_calibration/src/camera_calibration .
The goal is to be able to try various parameters available for the camera calibration with OpenCV, and to check whether those parameters may enhance the calibration. 

This package also regroups some advices to calibrate the intrinsics of the camera.

# How to use

# Calibration process

* Be sure that autofocus is not activated. For experiment, set autofocus at 60.

* Launch ROS camera topic; for this, you may use : 

roslaunch video_stream_opencv_mjpg webcam.launch 
(Check in launchfile that camID, height, width, fps, and codec, are correct with v4l2-ctl --list-devices and v4l2-ctl --list-formats-ext -d /dev/video[camID])

* Launch calibration process with : 

## For chessboard
rosrun camera_calibration cameracalibrator.py --size 6x8 --square 0.056 image:=/image_logitech_camera  --no-service-check

OR

rosrun camera_calibration cameracalibrator.py --size 4x6 --square 0.05 image:=/webcam/image_raw  --no-service-check

rosrun camera_calibration cameracalibrator.py --size 3x5 --square 0.044 image:=/image_logitech_camera  --no-service-check

rosrun camera_calibration cameracalibrator.py --size 3x5 --square 0.016 image:=/image_logitech_camera  --no-service-check


## For Charuco
rosrun camera_calibration cameracalibrator.py --pattern charuco --charuco_marker_size 0.022 --aruco_dict 6x6_250 --size 5x7 --square 0.036  image:=/image_logitech_camera  --no-service-check

OR:

rosrun camera_calibration cameracalibrator.py --pattern charuco --charuco_marker_size 0.046 --aruco_dict 4x4_250 --size 9x6 --square 0.059  image:=/image_logitech_camera  --no-service-check

NB : count of rows/ cols for Charuco : starting from aruco tag 0 (oriented in origin direction) : --size [number of tiles in cols]x[number of tiles in rows]

NB : count of rows/ cols for Chessboard : inside corners of chessboard in rows x inside corners of chessboard in cols 


WARNING : For Charuco, use Opencv <= 4.5.5 if your chessboard starts with an Aruco code instead of a black tile : https://github.com/opencv/opencv_contrib/issues/3291 ; https://github.com/opencv/opencv/pull/23153

NB : Charuco still not working. Todo to check : 
https://docs.opencv.org/3.4/df/d4a/tutorial_charuco_detection.html
https://github.com/kyle-bersani/opencv-examples/blob/master/CalibrationByCharucoBoard/CalibrateCamera.py
https://gitlab.tue.mpg.de/msafroshkin/oslab_aruco_calib/-/blob/54e1b98ed694a1230086578265395e9b5c7efada/aruco/tutorials/charuco_detection/charuco_detection.markdown
https://github.com/nullboundary/CharucoCalibration/blob/master/charucoBoardCalibration.py

Check notably : 
    * Objects points / images points. What are they? 
    * FInd datas about charuco calibration?


https://github.com/opencv/opencv_contrib/issues/3175



* Use advices from :

https://visp-doc.inria.fr/doxygen/visp-daily/tutorial-calibration-intrinsic.html 

and 

http://wiki.ros.org/camera_calibration/Tutorials/MonocularCalibration

and

https://stackoverflow.com/questions/11918315/does-a-smaller-reprojection-error-always-means-better-calibration

For all parameters : 

X : goto left and right
Y : up and down
skew : set to different rotation angles
size : start close from camera, and then go further from camera

Procedure : 

* Start close from camera, not parallal to image plane
* Move left to right, with different angles (never parallal to camera), then up and down
* When you will have finished your movement at a certain depth to camera, go further from camera, and redo the same left-right / up-down.
* Calibrate when everything is green.

* After calibration, check the "lin" number (corresponding to linear error of current chessboard) that appears. If its value is small enough for different poses, you may save it.

* Take file from /tmp/calibrationdata.tar.gz, save it into config_files folder.

* Extract ost.yaml file.

* Set in gst_ros.launch the path to ost.yaml

NB : d'après https://github.com/opencv/opencv_contrib/blob/4.x/modules/aruco/src/aruco_calib.cpp , calibrateCameraCharuco n'ajoute RIEN comme technique de calib. Utilise simplement cameraCalibration. Les marqueurs sont simplement utilisés pr avoir une meilleure estimation des correspondances entre les positions des coins sur l'image et les coins réels (dc, éviter les erreurs de symétrie) ou encore pour estimer la pose de la mire malgré certains occlusions. Typiquement, il n'y a pas d'utilisation d'éventuelles informations sur la pose de ces marqueurs pour mieux déterminer leur position 3D :(

NB : d'après https://docs.opencv.org/3.4/da/d13/tutorial_aruco_calibration.html, les marqueurs Charuco sont à préférer aux marqueurs Aruco.

NB :     *( Idée test pr optimiser intrinsèques à partir d'extrinsèques + Optitrack :
        - A partir rigidBodyOpti sur caméra, obtenir estimation de position screen_frame ds Opti
        - Utiliser planche calib ac marqueurs dessus.
        - A partir de planche calib, utiliser SolvePnP (https://docs.opencv.org/4.x/d9/d0c/group__calib3d.html#ga549c2075fac14829ff4a58bc931c033d) ac :
            * Image planche calib
            * Estimation extrinsèques ac Optitrack + useExtrinsicsGuess
        - Pbme optimisation où on cherche à opti : 
            - Intrinsèques
            - Transfo rigidBodyOpti -> SF 
        Tq ces deux matrices restent cstes quelque soit les prises de vue.
        ==> galère, pas ouf
    )


## Optimisation process

* Extract .jpg files from calibrationdata.tar.gz

* Put the path to those jpg files into calib.launch of camera_intrinsics_calib. This path is called next jpg_path.

* Launch node with : 
roslaunch camera_intrinsics_calib calib.launch 

* Call optimization of calibration with :
rosservice call /calib_intrinsics "[params]"

With [params] to be set as into next session.

* calibration_file (if option set) will be put into jpg_path/../ost_opti.yaml.

## Service

Parameters are : 

* parser.add_option("--mono", default=True, action="store_true", dest="mono",
                help="Monocular calibration only. Calibrates left images.")
*parser.add_option("-s", "--size", default=[], action="append", dest="size",
                help="specify chessboard size as NxM [default: 8x6]")
* parser.add_option("-q", "--square", default=[], action="append", dest="square",
                help="specify chessboard square size in meters [default: 0.108]")
* parser.add_option("--upload", default=False, action="store_true", dest="upload",
                help="Upload results to camera(s).")
* parser.add_option("--fix-principal-point", action="store_true", default=False,
                help="fix the principal point at the image center")
    - fpp#1
* parser.add_option("--fix-aspect-ratio", action="store_true", default=False,
                help="enforce focal lengths (fx, fy) are equal")
    - far#1
* parser.add_option("--zero-tangent-dist", action="store_true", default=False,
                help="set tangential distortion coefficients (p1, p2) to zero")
    - ztd#1
* parser.add_option("-k", "--k-coefficients", type="int", default=2, metavar="NUM_COEFFS",
                help="number of radial distortion coefficients to use (up to 6, default %default)")
* parser.add_option("--visualize", action="store_true", default=False,
                help="visualize rectified images after calibration")
* parser.add_option("-a", "--alpha", type="float", default=1.0, metavar="ALPHA",
                help="zoom for visualization of rectifies images. Ranges from 0 (zoomed in, all pixels in calibrated image are valid) to 1 (zoomed out, all pixels in  original image are in calibrated image). default %default)")
* parser.add_option("-m", "--max_chessboard-speed", type="float", default=-1, help="max_chessboard_speed" )

String format for service is |key_i#value_i|key_i+1#value#i+1.
Please check the .py file to get the keys. Booleans must be sent as integers.

For start, you may try : 

rosservice call /calib_intrinsics "v#1|msc#2000|fpp#0|far#0|ztd#0"

Best result for now : 

rosservice call /calib_intrinsics "v#0|msc#1000|fpp#1|far#0|ztd#0"

For saving : 

rosservice call /calib_intrinsics "v#0|msc#1000|fpp#1|far#0|ztd#0|sy#1"

# Evaluation of calibration

Can be done though two means : 

* Verbose option of service in calib.launch : v#1

* test.launch with calibration file.

## Good calibration

### In test_camera_2d_3d (empiric criterias)

* Max error and median errors are the same in the two figures (and in terminal).

# Extrinsics calibration process

## Get camera transform from optical_frame to optitrack_rigid_body_on_optical_frame

Do : 

~~~
roslaunch panda_qp_simple_control control_tracking.launch sim:=false load_gripper:=false load_calibration_tip:=true
~~~

(with FCI activated, and stop activated. This is just to launch the spawn of panda)

Then : 
~~~
roslaunch camera_intrinsics_calib calib_with_optitrack.launch 
~~~

Do classic calibration.

Then, save transform with : 

~~~
rosservice call /R2TF_save_tf "logitech#cam_frame"
~~~

Which will be saved in $(find robot_to_frame)/tf_transf/logitech_to_cam_frame.yaml

You can now use this transform with complete_hardware.launch

If you want to use it as a static camera_pose, set cam_frame as cam_frame_move.

If you rather want to use simple filtering, set logitech as logitech_filt, and use filtering package in moveit_prepros.launch .

If your transform is not good enough, you may need to change the .yaml by hand.
To do so, just launch a first time velocity_control_tracking.launch / panda_moveit_management.launch, then launch hardware_ws / preprocessing_ws packages. Each time you modify, you will just need to relaunch hardware_ws / preprocessing_ws packages.